/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.User;
import util.DB;

/**
 *
 * @author Gibran
 */
public class CustomerHandler{
    
    private static ArrayList<User> mCustomers = null;
       
    
    public static ArrayList<User> get(){
        CustomerHandler.set();
        return mCustomers;
    }
    private static void set(){
        mCustomers = mCustomers == null ? new ArrayList<User>() : mCustomers;
        mCustomers.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM customer ");
            while(result.next()){
                User customer = new User(result.getInt("customer_id"), result.getString("name"), result.getString("email"), result.getString("pass"));
                mCustomers.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static boolean save(User customer){
        String sql = "INSERT INTO CUSTOMER(name, email, pass) VALUES('" +
                customer.getName() + "','" +
                customer.getEmail() + "','" +
                customer.getPass() + "')";
        return DB.getInstance().execStatment(sql);
    }
    
}
