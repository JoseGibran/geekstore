package models;

public class User {
    private int customer_id;
    private int tipo_usuario;
    private String name;
    private String email;
    private String pass;

    public User(int customer_id, String name, String email, String pass) {
        this.customer_id = customer_id;
        this.name = name;
        this.email = email;
        this.pass = pass;
    }
    public User(){}

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    
}
