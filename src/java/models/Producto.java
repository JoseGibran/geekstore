
package models;


public class Producto {
   private int cod;
   private String desc;
   private String tipo;

    public Producto(int cod, String desc, String tipo) {
        this.cod = cod;
        this.desc = desc;
        this.tipo = tipo;
    }

    public Producto() {
    
    }
    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
}
