/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import handlers.CustomerHandler;
import models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import util.DB;



 
@Controller

public class HomeController {
    
    
    
    @RequestMapping("/")
    public String home(ModelMap modelMap) {
        
      //  CustomerHandler.save(new User(0, "Gibran", "gibran@royale.com", "123"));
       // System.out.println(CustomerHandler.get());
        modelMap.put("printme", "Hello Spring !!");
        return "home";
    }
    
    
}