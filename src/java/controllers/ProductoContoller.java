package controllers;

import java.util.ArrayList;
import java.util.List;
import models.Producto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductoContoller {
    List<Producto> miLista = new ArrayList<>();
    

    @RequestMapping(method = RequestMethod.GET, value = "/producto")
     public String productForm(){
         return "producto";
     }
   
       
 @RequestMapping(method = RequestMethod.POST, value = "/producto/store")
    public String productoStore(
        ModelMap modelMap,
        @RequestParam("codigo") String codigo,
        @RequestParam("descripcion") String descripcion, 
        @RequestParam("tipo") String tipo
    
    ){  
       Producto producto=new Producto();

         int numero= Integer.parseInt(codigo);
         producto.setCod(numero);
         producto.setDesc(descripcion);
         producto.setTipo(tipo);
        
         miLista.add(producto);
         modelMap.put("productos", miLista);
         return "lista_producto";      

    }
    
    
     @RequestMapping(method = RequestMethod.GET, value="/producto/lista")
    public String producto(ModelMap modelMap) {
         modelMap.put("productos", miLista);
        return "lista_producto";
    }
}
