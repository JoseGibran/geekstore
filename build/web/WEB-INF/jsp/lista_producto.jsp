<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                     <h1>Lista Producto</h1>
          <table class="table">
            
            <thead>
              <div align="right">  <a type="button" class="btn btn-success" href="http://localhost:8080/geekstore/producto">Nuevo</a></div>
                <th>
                   Codigo: 
                </th>
                <th>
                   Descripcion:
                </th>
                <th>
                   Precio:
                </th>
            </thead>
             <tbody>
                <c:forEach var="producto" items="${productos}">
                    <tr>
                            <td>${producto.getCod()}</td>
                            <td>${producto.getDesc()}</td>
                            <td>${producto.getTipo()}</td>
                              <td><button type="button" class="btn btn-primary">Primary</button></td>
                              <td><button type="button" class="btn btn-danger">Primary</button></td>
                        </tr>              
                </c:forEach>

            </tbody>
            
        </table>
                </div>
            </div>
        </div>    
    </body>
</html>
